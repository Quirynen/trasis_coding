﻿using System;
using System.Collections.Generic;
using System.Text;
using TrasisRockPaperScissor.Model;

namespace TrasisRockPaperScissor
{
    public class Game
    {
        private readonly List<Player> _players;

        public Game(List<Player> players)
        {
            _players = players ?? throw new ArgumentNullException(nameof(players));
        }

        public Player PlayGame()
        {
            Player winner = _players[0];
            while (_players.Count > 1)
            {
                for (int i = 0; i < _players.Count - 1; i++)
                {
                    winner = GetWinner(_players[i], _players[i + 1]);
                }
            }

            return winner;
        }

        public Player GetWinner(Player p1, Player p2)
        {
            // Sign are the same, get winner with lowest number
            Player winner;
            if (p1.Sign.Equals(p2.Sign))
            {
                winner = p1.Number < p2.Number ? AddPlayerFaced(p1, p2) : AddPlayerFaced(p2, p1);
            }
            else
            {
                winner = p1.Sign._defeat.Contains(p2.Sign._sign) ? AddPlayerFaced(p1, p2) : AddPlayerFaced(p2, p1);
            }

            return winner;
        }

        private Player AddPlayerFaced(Player winner, Player loser)
        {
            winner.PlayersFaced.Add(loser);
            _players.Remove(loser);
            return winner;
        }
    }
}
