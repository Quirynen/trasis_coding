﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TrasisRockPaperScissor.Model
{
    public class Sign
    {
        public string _sign;
        public List<string> _defeat;

        public Sign(string sign)
        {
            _sign = sign;
            _defeat = GetDefeatedSigns(sign);
        }

        private List<string> GetDefeatedSigns(string sign)
        {
            switch (sign)
            {
                case "R":
                    return new List<string> { "C", "L" };
                case "P":
                    return new List<string> { "R", "S" };
                case "C":
                    return new List<string> { "P", "L" };
                case "L":
                    return new List<string> { "S", "P" };
                case "S":
                    return new List<string> { "R", "C" };
                default:
                    return null;
            }
        }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Sign s = (Sign)obj;
                return _sign == s._sign && _defeat.SequenceEqual(s._defeat);
            }
        }
    }
}
