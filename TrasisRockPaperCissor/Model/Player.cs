﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrasisRockPaperScissor.Model
{
    public class Player
    {
        public int Number { get; set; }
        public Sign Sign { get; set; }

        public List<Player> PlayersFaced { get; set; }

        public Player(int number, Sign sign)
        {
            Number = number;
            Sign = sign;
            PlayersFaced = new List<Player>();
        }

        public override string ToString()
        {
            return "Player number " + Number + " with sign " + Sign._sign;
        }

    }
}
