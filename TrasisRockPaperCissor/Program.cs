﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrasisRockPaperScissor.Model;

namespace TrasisRockPaperScissor
{


    /**
     * Auto-generated code below aims at helping you parse
     * the standard input according to the problem statement.
     **/
    class Program
    {
        static void Main(string[] args)
        {
            List<Player> players = new List<Player>();
            Console.WriteLine("Enter the numbers of players for this awesome game : ");
            int N = int.Parse(Console.ReadLine());

            //Check input for N
            if (N < 2 || N > 1024 || !IsPowerOfTwo((ulong)N))
            {
                Console.WriteLine("N should be >=2 and <= 1024 and be a power of 2, check your input.");
                Environment.Exit(0);
            }
            for (int i = 0; i < N; i++)
            {
                Console.WriteLine($"Enter data for player {i + 1} :");
                // We don't check for the input as we believe user plays it nice with us
                string[] inputs = Console.ReadLine().Split(' ');
                int numberPlayer = int.Parse(inputs[0]);
                Player player = new Player(numberPlayer, new Sign(inputs[1]));
                if (players.Any(x => x.Number == numberPlayer))
                {
                    throw new ArgumentException("Number already taken by one of the players");
                }

                players.Add(player);
            }
            Game game = new Game(players);
            Player winner = game.PlayGame();

            Console.WriteLine($"The winner is {winner.Number} and faced these opponents : {string.Concat(winner.PlayersFaced.Select(o => o.ToString() + '/'))}");
        }

        static bool IsPowerOfTwo(ulong x)
        {
            return (x != 0) && ((x & (x - 1)) == 0);
        }
    }
}
