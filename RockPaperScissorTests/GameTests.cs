using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TrasisRockPaperScissor;
using TrasisRockPaperScissor.Model;

namespace RockPaperScissorTests
{
    public class GameTests
    {
        [Test]

        public void Player_2_Should_Win_The_Game_And_Have_Faced_5_6_1()
        {
            //Arrange
            var players = new List<Player>
            {
                new Player(4, new Sign("R")), new Player(1, new Sign("P")), new Player(8, new Sign("P")), new Player(3, new Sign("R")),
                new Player(7, new Sign("C")), new Player(5, new Sign("S")), new Player(6, new Sign("L")), new Player(2, new Sign("L"))
            };
            var game = new Game(players);

            //Test
            var winner = game.PlayGame();

            //Assess
            Assert.AreEqual(2, winner.Number);
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 6));
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 5));
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 1));
        }

        [Test]
        public void Player_With_Lower_Number_Should_Win_The_Game()
        {
            //Arrange
            var players = new List<Player> { new Player(1, new Sign("R")), new Player(4, new Sign("R")), new Player(6, new Sign("R")), new Player(3, new Sign("R")) };
            var game = new Game(players);

            //Test
            var winner = game.PlayGame();

            //Assess
            Assert.AreEqual(1, winner.Number);
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 4));
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 3));
        }

        [Test]

        public void Paper_Should_Win_The_Game()
        {
            //Arrange
            var players = new List<Player>
            {
                new Player(1, new Sign("R")), new Player(4, new Sign("R")), new Player(6, new Sign("R")), new Player(3, new Sign("R")),
                new Player(15, new Sign("P")), new Player(14, new Sign("R")), new Player(16, new Sign("R")), new Player(13, new Sign("R"))
            };

            var game = new Game(players);

            //Test
            var winner = game.PlayGame();

            //Assess
            Assert.AreEqual(15, winner.Number);
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 14));
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 13));
            Assert.That(winner.PlayersFaced.Any(x => x.Number == 1));
        }
    }
}